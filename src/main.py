import asyncio
from aiohttp import ClientSession
from typing import List
from schema import Item
from datetime import datetime
import csv
import orjson
import aiofiles
from aiocsv import AsyncDictWriter

from loguru import logger
import backoff
from aiohttp import client_exceptions

headers = {
    'Host': 'api-prod.retailsso.com',
    'appversion': '196',
    'env': 'PROD',
    'langcode': 'en',
    'storeid': 'mafbhr',
    'userid': 'petr777@yahoo.com',
    'deviceid': '869a1920-9f8a-4372-ac3d-0dee70c8cadd',
    'appid': 'Android',
    'osversion': '22',
    'currency': 'BHD',
    'token': 'f-6cdg4wj577e7Rj0_Ni8AhjxZ8',
    'user-agent': 'okhttp/4.8.1',
}

@backoff.on_exception(backoff.expo,
                      client_exceptions.ContentTypeError,
                      max_tries=3)
async def get_main_category(session: ClientSession) -> List[dict]:
    params = {
        'areacode': 'Budaiya - Northern Governorate',
        'componentId': 'aggregator',
    }
    url = 'https://api-prod.retailsso.com/v2/stores/mafbhr/en/cms/component'
    response = await session.get(url, params=params)
    data = await response.json()
    food_categories = data.get('foodCategories')
    return [
        {'id': category.get('id'), 'name': category.get('name')}
        for category in food_categories
    ]

@backoff.on_exception(backoff.expo,
                      client_exceptions.ContentTypeError,
                      max_tries=3)
async def get_sub_category(session: ClientSession, category: dict):

    params = {
        'areacode': 'Budaiya - Northern Governorate',
        'parentCategory': category.get('id'),
    }
    url = 'https://api-prod.retailsso.com/v2/categories/mafbhr/en'
    response = await session.get(
        url = url,
        params = params
    )
    data = await response.json()
    return [
        {'id': category.get('id'), 'name': category.get('name')}
        for category in data.get('subcategories')
    ]

@backoff.on_exception(backoff.expo,
                      client_exceptions.ContentTypeError,
                      max_tries=3)
async def get_products(session: ClientSession, category_id: str):

    params = {
        'filter': '',
        'sortBy': '',
        'currentPage': '0',
        'pageSize': '999',
        'maxPrice': '',
        'minPrice': '',
        'areaCode': 'Budaiya - Northern Governorate',
        'lang': 'en',
        'nextOffset': '',
        'disableSpellCheck': 'true',
    }

    url = f'https://api-prod.retailsso.com/v3/categories/mafbhr/en/categories/{category_id}'
    response = await session.get(
        url = url,
        params = params
    )
    data = await response.json()

    products = data.get('data', {}).get('products')
    return products

@backoff.on_exception(backoff.expo,
                      client_exceptions.ContentTypeError,
                      max_tries=3)
async def parser_categories(session: ClientSession):

    main_categories = await get_main_category(session)

    for main_category in main_categories:

        sub_categories = await get_sub_category(session, main_category)

        for sub_category in sub_categories:
            extreme_categories = await get_sub_category(session, sub_category)

            for extreme_category in extreme_categories:
                yield extreme_category


async def save_products(products):

    for product in products:

        images = product.get('links', {}).get('images')
        url_images = [img.get('href') for img in images]

        price = product.get('price', {})
        discount = price.get('discount', {})
        price_old = product.get('price', {}).get('price')

        item = Item(
            sku = product.get('id'),
            name = product.get('name'),
            url = product.get('productUrl', {}).get('href'),
            img = url_images[0],
            price_old = price_old if price != price_old else None,
            price = discount.get('price') or price.get('price'),
            updated = datetime.now(),
            stock = product.get('stock', {}).get('value')
        )

        yield item




async def start():
    fieldnames = list(Item.schema()["properties"].keys())

    async with ClientSession(headers = headers) as session:

        async with aiofiles.open("carrefour.csv", mode="w", encoding="utf-8", newline="") as afp:
            writer = AsyncDictWriter(afp, fieldnames, restval="NULL", quoting=csv.QUOTE_ALL)
            await writer.writeheader()

            async for category in parser_categories(session):
                total = 0
                log_str = category.get('name')
                products = await get_products(session, category.get('id'))

                async for product in save_products(products):
                    try:
                        await writer.writerow(orjson.loads(product.json()))
                        total += 1
                    except Exception as err:
                        logger.error(err)
                log_str += f' {total} lines added to the file'
                logger.info(log_str)
                #await asyncio.sleep(0.1)


if __name__ == '__main__':
    asyncio.run(start())
