from pydantic import BaseModel, HttpUrl, validator
from datetime import datetime


class Item(BaseModel):
    sku: str
    name: str
    description: str = None
    url: HttpUrl = None
    img: HttpUrl = None
    price_old: float = None
    price: float = None
    updated: datetime = None
    stock: int = None